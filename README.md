# Blockland-Unlimited-Mining-Classic
 A rework of the classic Unlimited Mining game-mode for Blockland. Includes fixes for many known bugs and issues in the original. The goal of this project is simply to fix up the game-mode while maintaining the classic gameplay.

# Contributing
Contributions via pull requests are welcome and greatly appreciated. Just keep in mind a few guidelines
- Code should be clean and manageable.
- Please stick to fixes and minor tweaks that maintain the classic experience. If you'd like to add major new features, consider forking this project and making your own variant instead.
